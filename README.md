# GatorCode

GatorCode is a platform for University of Florida students to learn and practice their coding skills. Apart from companies in the technology sector, the number of businesses relying on computer code is increasing drastically. Due to this, programming has become a favorable skill in almost any profession. GatorCode gives all UF students the opportunity to practice coding in Java, Python, C, and C++ with interactive lessons and challenges. Also, users can create "Sandboxes" where they have the ability to get creative with thier coding skills. Also, users can compete with thier fellow Gators by earning points through completeing lessons, challenges, and participating in competitions.

## Utilized Technologies

NodeJS, ReactJS, MongoDB, Linux/Unix

## Installation

1. Clone the GatorCode repo.

```bash
git clone https://gitlab.com/gatorcode/gatorcode.git
```

2. Run the server (this will connect you to the GatorCode server and MongoDB database)

```bash
cd server
npm run dev
```

3. Run the compiler (this will allow you to run code)

```bash
cd compiler
npm run dev
```

4. Run the client (this will allow you to view the GatorCode front-end in the browser)

```bash
cd client
npm start
```

## Contibute

- Fork the repostory
- Install dependencies
- Make your changes
- Make a pull request from your repo

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Our Team

Kerrick Cavanaugh - Founder/Co-President/Lead Developer <br />
Marc Miller - Co-President/Full-Stack Developer <br />
Daniela Fragoso - Front-End (React) Developer <br />
