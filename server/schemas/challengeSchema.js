const mongoose = require("mongoose");

const challenge = new mongoose.Schema({
  difficulty: { type: String, required: true },
  major: { type: Array, required: true },
  title: { type: String, required: true, unique: true },
  intro: { type: String, required: true },
  desc: { type: String, required: true },
  figure: { type: String, required: false },
  correctOutput: { type: String, required: true },
  lessons: { type: Array, required: true },
  example1: { type: String, required: false },
  example2: { type: String, required: false },
  example3: { type: String, required: false },
  hint1java: { type: String, required: true },
  hint2java: { type: String, required: true },
  hint3java: { type: String, required: true },
  hint1cpp: { type: String, required: true },
  hint2cpp: { type: String, required: true },
  hint3cpp: { type: String, required: true },
  hint1c: { type: String, required: true },
  hint2c: { type: String, required: true },
  hint3c: { type: String, required: true },
  hint1python: { type: String, required: true },
  hint2python: { type: String, required: true },
  hint3python: { type: String, required: true },
  testcases: { type: Array, required: true },
  inputjava: { type: String, required: true },
  inputpython: { type: String, required: true },
  inputcpp: { type: String, required: true },
  inputc: { type: String, required: true },
  inputType: { type: String, required: true }
});

module.exports = mongoose.model("Challenge", challenge);
