const mongoose = require("mongoose");

const lesson = new mongoose.Schema({
  section: { type: String, required: true },
  title: { type: String, required: true, unique: true },
  task: { type: String, required: true, unique: true },
  descjava: { type: String, required: true },
  desccpp: { type: String, required: true },
  descpython: { type: String, required: true },
  descc: { type: String, required: true },
  video: { type: String, required: false },
  correctOutput: { type: String, required: true },
  testsjava: { type: Array, required: true },
  testspython: { type: Array, required: true },
  testscpp: { type: Array, required: true },
  testsc: { type: Array, required: true },
  inputjava: { type: String, required: true },
  inputpython: { type: String, required: true },
  inputcpp: { type: String, required: true },
  inputc: { type: String, required: true },
  inputType: { type: String, required: true },
  solutionjava: { type: String, required: false },
  solutioncpp: { type: String, required: false },
  solutionc: { type: String, required: false },
  solutionpython: { type: String, required: false }
});

module.exports = mongoose.model("Lesson", lesson);
