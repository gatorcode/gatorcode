const mongoose = require("mongoose");

const competition = new mongoose.Schema({
  major: { type: Array, required: false },
  live: { type: Boolean, required: true },
  registration_active: { type: Boolean, required: true },
  title: { type: String, required: true, unique: true },
  intro: { type: String, required: true },
  date: { type: String, required: true },
  regClosing: { type: String, required: true },
  desc: { type: String, required: true },
  registeredUsers: { type: Array, required: true },
  figure: { type: String, required: false },
  correctOutput: { type: String, required: true },
  input: { type: String, required: true },
  shortDesc: { type: String, required: true },
  firstPrize: { type: String, required: true },
  secondPrize: { type: String, required: true },
  thirdPrize: { type: String, required: true }
});

module.exports = mongoose.model("Competition", competition);
