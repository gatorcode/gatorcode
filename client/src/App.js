import React from "react";
import "./App.css";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Axios from "axios";
import Homepage from "./pages/homepage";
import Sandbox from "./pages/sandbox";
import Navbar from "./components/navbar";
import Login from "./pages/login";
import Register from "./pages/register";
import Leaderboards from "./pages/leaderboards";
import Lessons from "./pages/lessons";
import LessonPages from "./pages/lessonPages";
import Challenges from "./pages/challenges";
import Challenge from "./pages/challenge";
import Contact from "./pages/contact";
import Settings from "./pages/settings";
import Competitions from "./pages/competition";
import Competition from "./pages/competition-live";
import { API_URL } from "./constants";

function App() {
  // const isLoggedin = async () => {
  //   const getuser = getUser();
  //   const userdata = await getuser;
  //   if (userdata.data && localStorage.getItem("usl") === "aaa") {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  const getUser = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: `${API_URL}/getuser`
    });
  };

  return (
    <Router>
      <div>
        <Navbar data={getUser()} />
        <Switch>
          <Route path="/" exact component={Homepage} />
          <Route
            path="/lessons"
            exact
            render={props => <Lessons {...props} data={getUser()} />}
          />
          <Route
            path="/lessonPages/:title"
            exact
            render={props => <LessonPages {...props} data={getUser()} />}
          />
          <Route
            path="/settings"
            exact
            render={props => <Settings {...props} data={getUser()} />}
          />

          <Route path="/contactus" exact component={Contact} />

          <Route
            path="/sandbox"
            exact
            render={props => <Sandbox {...props} data={getUser()} />}
          />
          <Route path="/login" exact component={Login} />
          <Route
            path="/challenges"
            exact
            render={props => <Challenges {...props} data={getUser()} />}
          />
          <Route
            path="/competitions"
            exact
            render={props => <Competitions {...props} data={getUser()} />}
          />
          <Route
            path="/competition/:title"
            exact
            render={props => <Competition {...props} data={getUser()} />}
          />
          <Route path="/register" exact component={Register} />
          <Route
            path="/lessons"
            exact
            render={props => <Lessons {...props} data={getUser()} />}
          />
          <Route
            path="/lessonPages/:title"
            exact
            render={props => <LessonPages {...props} data={getUser()} />}
          />
          <Route path="/leaderboards" exact component={Leaderboards} />
          <Route
            path="/challenge/:title"
            exact
            render={props => <Challenge {...props} data={getUser()} />}
          />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
