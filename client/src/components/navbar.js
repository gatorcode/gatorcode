import React, { useState, useEffect } from "react";
import "./styles/navbar.css";

import mainLogo from "../media/mainLogo.png";
import defaultProfilePic from "../media/avatars/default.png";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Navbar,
  Nav,
  DropdownButton,
  Dropdown,
  Col,
  ProgressBar
} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import { Link, BrowserRouter as Router } from "react-router-dom";
import Axios from "axios";
import avatar1 from "../media/avatars/1.png";
import { useHistory } from "react-router-dom";
import { API_URL } from "../constants";

const NavbarMain = props => {
  const history = useHistory();

  const [userdata, setUserdata] = useState(props.data);
  const [useravatar, setUseravatar] = useState(1);
  const [userfirstname, setUserfirstname] = useState(null);
  const [userlevel, setUserlevel] = useState(null);
  const [userPoints, setUserPoints] = useState(null);

  const [signedout, setSignedout] = useState(true);

  const signOut = () => {
    setUserdata("");
    setUserfirstname("");
    setSignedout(true);
    localStorage.setItem("usl", "bbb");
    clearLocalStorage();

    Axios({
      method: "POST",
      data: {
        username: userdata.username
      },
      withCredentials: true,
      url: `${API_URL}/logout`
    });

    history.push("/");

    //window.location.reload(false);
  };

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
  };

  const setImage = () => {
    switch (useravatar) {
      case 1:
        return <img id="profilePic" src={defaultProfilePic}></img>;
      case 2:
        return <img id="profilePic" src={avatar1}></img>;
      default:
        return <img id="profilePic" src={defaultProfilePic}></img>;
    }
  };

  const setItems = async () => {
    try {
      if (localStorage.getItem("usl") == "aaa") {
        const dataobj = await userdata;
        if (dataobj) {
          //console.log(dataobj);
          setUserlevel(dataobj.data.level);
          setUserfirstname(dataobj.data.firstname);
          setUseravatar(dataobj.data.avatar);
          setUserPoints(dataobj.data.points);
          setImage();
          //localStorage.setItem("usl", "aaa");
        }
      }
    } catch (err) {
      if (err) console.log(err);
    }
  };

  useEffect(() => {
    setItems();
  }, []);

  return (
    <React.StrictMode>
      <nav className="navBar">
        <Navbar collapseOnSelect expand="lg" bg="secondary" variant="dark">
          <a>
            <img id="mainLogo" src={mainLogo}></img>
          </a>
          <Navbar.Brand href="/">Gator Code</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Link className="navBarItems" to="/lessons">
                Lessons
              </Link>
              <Link className="navBarItems" to="/challenges">
                Challenges
              </Link>
              <Link className="navBarItems" to="/competitions">
                Competitions
              </Link>
              <Link className="navBarItems" to="/sandbox">
                Sandbox
              </Link>
              <Link className="navBarItems" to="/leaderboards">
                Leaderboards
              </Link>
              <Link className="navBarItems" to="/contactus">
                Contact
              </Link>

              <div className="next-comp card bg-dark text-white bg-secondary my-3 py-2 text-center">
                <div className="card-body">
                  <p className="text-white m-0">
                    Next Upcoming Competition: <b>02/01/2021</b>
                  </p>
                </div>
              </div>

              {/* Drop Down Menu with 3 items
              <NavDropdown className="navBarItems" title="Learn" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Interview</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Internship</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">More...</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
              </NavDropdown>
             */}
            </Nav>
            <Nav>
              {!userfirstname ? (
                <div>
                  <a href="/login">
                    <Button className="m-2" variant="outline-light">
                      Sign In
                    </Button>
                  </a>
                  <a href="/register">
                    <Button className="m-2" variant="light">
                      Sign Up
                    </Button>
                  </a>
                </div>
              ) : (
                <div className="navuserinfo">
                  <div className="levelInfo">
                    <p className="navBarItems" id="userlevel">
                      Level: <b>{userlevel}</b>
                    </p>
                    <ProgressBar
                      animated
                      now={userPoints}
                      max={userlevel * 5}
                      // label={userlevel}
                    />
                  </div>
                  <Col>
                    <div className="dropdownmenu">
                      <DropdownButton
                        id="dropdown"
                        drop="left"
                        title={setImage()}
                      >
                        <Dropdown.Item href="/settings">Settings</Dropdown.Item>
                        <Dropdown.Item onClick={signOut}>
                          Sign Out
                        </Dropdown.Item>
                      </DropdownButton>
                    </div>
                    <p className="navBarItems" id="userfirstname">
                      Hello, <b>{userfirstname}</b>
                    </p>
                  </Col>
                </div>
              )}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </nav>
    </React.StrictMode>
  );
};
export default NavbarMain;
