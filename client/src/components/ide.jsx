import React, { useState } from "react";
import { Row, Col, Dropdown } from "react-bootstrap";
import "./styles/ide.css";
import { COMPILER_PORT } from "../constants";
import { useEffect } from "react";
const axios = require("axios");

function Ide() {
  const [name, setName] = useState(
    localStorage.getItem("name") ? localStorage.getItem("name") : ""
  );
  const [result, setResult] = useState("Submit code to see result.");
  const [code, setCode] = useState(localStorage.getItem("selected"));
  const [input, setInput] = useState("");

  const lang = localStorage.getItem("lang");

  const onSubmitCode = async e => {
    e.preventDefault();

    let fileName = "";
    if (localStorage.getItem("selectedtitle")) {
      //if in sandbox
      fileName = localStorage.getItem("selectedtitle");
    } else if (document.getElementById("challengeTitle")) {
      //if in challenges
      fileName = `gcode.${localStorage.getItem("lang")}`;
    } else if (document.getElementById("lessonTitle")) {
      //if in lessons

      fileName = `gcode.${localStorage.getItem("lang")}`;
    }

    axios
      .post(`http://localhost:${COMPILER_PORT}/code/submit${lang}/`, {
        code: code,
        input: input,
        lang: lang,
        fileName: fileName
      })
      .then(res => {
        if (res.data.stderr) {
          setResult(res.data.stderr);
        } else if (res.data.shortMessage) {
          setResult(res.data.shortMessage);
        } else {
          setResult(res.data);
        }
      });
  };

  const [IdeTheme, setIdeTheme] = useState(
    `{"background": "gray", "color": "black"}`
  );

  const IdeThemeSetter = () => {
    switch (IdeTheme) {
      case `{"background": "gray", "color": "black"}`:
        return JSON.parse(`{"background": "gray", "color": "black"}`);
      case `{"background": "black", "color": "green"}`:
        return JSON.parse(`{"background": "black", "color": "green"}`);
    }
  };

  const renderActivatedSandbox = () => {
    return (
      <div className="ide-full">
        <div id="ide-main" className="ide-main">
          {rows}
        </div>
      </div>
    );
  };

  const [rows, setRows] = useState([]);

  useEffect(async () => {
    if (localStorage.getItem("selected")) {
      const initialVal = await localStorage.getItem("selected").split("\n");
      const initialRows = initialVal.map((i, j) => {
        return renderNewRow(j + 1, initialVal[j]);
      });
      setRows(initialRows);
    }
  }, []);

  const renderNewRow = (index, content) => {
    return (
      <div className="ide-row">
        <div className="line-number">
          <p>{index}</p>
        </div>
        <div className="line-content">
          <textarea
            style={{
              fontFamily:
                "source-code-pro, Menlo, Monaco, Consolas, `Courier New`, monospace",
              background: "gray"
            }}
            id={`row-${index}`}
            className="row-ide"
            cols="50"
            index={index}
            rows="1"
            spellCheck="false"
            autoCapitalize="false"
            autoCorrect="false"
            wrap="off"
            onChange={onCodeChange}
            //value={content}
          >
            {content}
          </textarea>
        </div>
      </div>
    );
  };

  const onCodeChange = e => {
    localStorage.setItem("selectedRow", `${e.target.id}`);
    const curRow = document.getElementById(localStorage.getItem("selectedRow"));
    if (curRow) {
      curRow.addEventListener("keydown", function(e) {
        let rnum = e.target.id.match(/\d+/)[0];

        //!add n spaces for the nth line
        if (e.key == "Enter") {
          e.preventDefault();
          let cde = code;
          console.log(cde);
          let cur = localStorage.getItem("selected").split("\n");

          //console.log(cur);

          cur.splice(parseInt(rnum), 0, "\n");

          //shift everything down
          // cur.map((i, j) => {
          //   let temp = cur[j];
          //   cur.splice(parseInt(rnum), 1, temp);
          // });

          const newrows = cur.map((i, j) => {
            return renderNewRow(j + 1, cur[j]);
          });

          setRows(newrows);

          let newcodestring = cur.join("\n");

          setCode(newcodestring);

          localStorage.setItem("selected", newcodestring);
          localStorage.setItem("code", newcodestring);

          document.getElementById(`row-${parseInt(rnum) + 1}`).focus();
        } else if (e.key === "Backspace" || e.key === "Delete") {
          //e.preventDefault();

          let rnum = e.target.id.match(/\d+/)[0];

          let cde = code;
          if (e.target.value === "" && rnum !== 1) {
            let cur = localStorage.getItem("selected").split("\n");
            cur.splice(parseInt(rnum), 1);

            const newrows = cur.map((i, j) => {
              return renderNewRow(j + 1, cur[j]);
            });

            setRows(newrows);

            let newcodestring = cur.join("\n");
            localStorage.setItem("selected", newcodestring);
            localStorage.setItem("code", newcodestring);
            document.getElementById(`row-${parseInt(rnum - 1)}`).focus();
          }

          //renderInitialRows();
        } else if (e.keyCode === 40) {
          //down key
          if (document.getElementById(`row-${parseInt(rnum) + 1}`)) {
            document.getElementById(`row-${parseInt(rnum) + 1}`).focus();
          }
        } else if (e.keyCoe === 38) {
          //up key
          if (document.getElementById(`row-${parseInt(rnum) - 1}`)) {
            document.getElementById(`row-${parseInt(rnum) - 1}`).focus();
          }
        }
      });
    }

    // //if tab pressed

    // //if code changed
    // //get current number of rows [textbox divs]
    // //loop through each textbox
    // //combine all content
    // //set code
    // let numRows = 0;
    // let i = 1;
    // while (document.getElementById(`row-${i}`)) {
    //   numRows++;
    //   i++;
    // }

    // let curCode = "";
    // for (let i = 1; i <= numRows; i++) {
    //   curCode += document.getElementById(`row-${i}`).value;
    //   curCode += `\n`;
    // }
    // //console.log(curCode);
    // localStorage.setItem("code", curCode);
    // setCode(localStorage.getItem("code"));
  };

  const onInputChange = e => {
    setInput(e.target.value);
  };

  return (
    <div className="ide-full">
      <Row>
        <Col>
          <div className="idemain">
            {name == "" && document.getElementById("sandselect") ? (
              <React.Fragment>
                <div className="">
                  <textarea
                    type="text"
                    id="code"
                    placeholder="Please select a sandbox."
                    onChange={onCodeChange}
                    rows="30"
                    cols="50"
                    autoFocus="true"
                    disabled="true"
                    spellCheck="false"
                    autoCapitalize="false"
                    autoCorrect="false"
                    wrap="off"
                    value={rows}
                  ></textarea>
                </div>
                <div className="">
                  <p className="lead d-block my-0">
                    Input (Separated by commas)
                  </p>
                  <textarea
                    type="text"
                    id="input"
                    placeholder="Please select a sandbox."
                    onChange={onInputChange}
                    disabled="true"
                  ></textarea>
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {renderActivatedSandbox()}
                <div className="input">
                  <p className="lead d-block my-0">
                    Input (Separated by commas)
                  </p>
                  <textarea
                    type="text"
                    id="input"
                    placeholder="cat,5,[75,67,13,1]"
                    value={input}
                    onChange={onInputChange}
                  >
                    {input}
                  </textarea>
                </div>
              </React.Fragment>
            )}

            <button
              id="submitbutton"
              className="btn btn-success"
              onClick={onSubmitCode}
            >
              Submit Code
            </button>
            <div className="row">
              <div className="output">
                <p className="lead d-block my-0">Output</p>
                <textarea
                  type="text"
                  id="result"
                  disabled={false}
                  value={result}
                ></textarea>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default Ide;
