import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { API_URL } from "../constants";
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button, Container, Jumbotron } from "react-bootstrap";
//import mainLogo from "../media/mainLogo.png";

function Login() {
  const [loginUsername, setLoginUsername] = useState("");
  const [loginPassword, setLoginPassword] = useState("");
  const history = useHistory();
  //const [data, setData] = useState({});
  //const [loggedIn, setLoggedIn] = useState(false);

  const loginUser = () => {
    Axios({
      method: "POST",
      data: {
        username: loginUsername,
        password: loginPassword
      },
      withCredentials: true,
      url: `${API_URL}/login`
    })
      .then(localStorage.setItem("usl", "aaa"))
      .then(history.push("/"));
  };

  // useEffect(() => {
  //   if (localStorage.getItem("usl") === "aaa") {
  //     // clearLocalStorage();
  //     // setData();
  //     history.push("/");
  //   }
  // }, []);

  return (
    <React.StrictMode>
      <div class="container">
        <Jumbotron className="headerReg">
          <h1 id="titleh1">GatorCode</h1>
          <p>Code the Gator way.</p>
        </Jumbotron>
      </div>

      <div>
        <Container>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Username"
              onChange={e => setLoginUsername(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="Password"
              placeholder="Password"
              onChange={e => setLoginPassword(e.target.value)}
            />
          </Form.Group>

          <Button onClick={loginUser}>Submit</Button>
          <Container>
            <h4>Dont have a GatorCode account?</h4>
            <Button variant="secondary" href="./register">
              Register Now
            </Button>
          </Container>
        </Container>
      </div>
      <div className="loginPic"></div>
    </React.StrictMode>
  );
}

export default Login;
