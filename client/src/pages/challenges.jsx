import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import "./styles/challenges.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, ListGroup } from "react-bootstrap";
import Axios from "axios";
import { MdCheckCircle } from "react-icons/md";
import { FaClock } from "react-icons/fa";
import { API_URL } from "../constants";

//FaRegCheckCircle
function Challenges(props) {
  const history = useHistory();

  const [userdata, setUserdata] = useState(props.data);
  const [challengesCompleted, setChallengesCompleted] = useState(props.data);
  const [challengeMap, setChallengeMap] = useState([]);

  const [selectedMajor, setSelectedMajor] = useState("");

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(async () => {
    clearLocalStorage();
    const propuserdata = await userdata;
    const challenges = await propuserdata.data.challengesCompleted;
    console.log(challenges);
    setChallengesCompleted(challenges);
    // setSelectedMajor(propuserdata.data.major);
    setSelectedMajor("All");
  }, []);

  const getAllChallenges = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: `${API_URL}/getallchallenges`
    });
  };

  const setData = async () => {
    const data = await getAllChallenges();
    setChallengeMap(data.data);
  };

  // const [status, setStatus] = useState("");

  // const challengeComplete = async title => {
  //   const dataobj = await userdata;
  //   console.log(dataobj);
  //   const completedChallenges = dataobj.challengesCompleted;

  //   switch (completedChallenges[title]) {
  //     case 1:
  //       setStatus("Completed");
  //     case 2:
  //       setStatus("Attempted");
  //   }
  // };
  const showStatus = (status, title) => {
    console.log(status);
    var component;
    if (status === 2) {
      component = <MdCheckCircle color="green" />;
    } else if (status === 1) {
      component = <FaClock color="yellow" />;
    }
    return (
      <div>
        {title} {component}
      </div>
    );
  };
  const showEasyChallenges = () => {
    if (selectedMajor !== "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;
        const major = challengeMap[j].major;

        if (difficulty === "Easy" && major.includes(selectedMajor)) {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="success"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    } else if (selectedMajor === "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;
        // console.log(challengesCompleted[challengeMap[j].title])
        if (difficulty === "Easy") {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="success"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    }
  };

  const showMediumChallenges = () => {
    if (selectedMajor !== "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;
        const major = challengeMap[j].major;
        if (difficulty === "Medium" && major.includes(selectedMajor)) {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="warning"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    } else if (selectedMajor === "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;

        if (difficulty === "Medium") {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="warning"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    }
  };

  const showHardChallenges = () => {
    if (selectedMajor !== "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;
        const major = challengeMap[j].major;
        if (difficulty === "Hard" && major.includes(selectedMajor)) {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="danger"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    } else if (selectedMajor === "All") {
      return challengeMap.map((i, j) => {
        const difficulty = challengeMap[j].difficulty;

        if (difficulty === "Hard") {
          return (
            <React.Fragment>
              <ListGroup.Item
                className="challengeCard"
                action
                href={`/challenge/${challengeMap[j].title}`}
                variant="danger"
              >
                {showStatus(
                  challengesCompleted[challengeMap[j].title],
                  challengeMap[j].title
                )}
              </ListGroup.Item>
            </React.Fragment>
          );
        }
      });
    }
  };

  const handleLangChange = e => {
    const langVal = e.target.value;
    localStorage.setItem("lang", langVal);
  };

  const handleMajorChange = e => {
    const majorVal = e.target.value;
    localStorage.setItem("selectedMajor", majorVal);
    setSelectedMajor(majorVal);
  };

  useEffect(async () => {
    if (localStorage.getItem("usl") === "aaa" && userdata) {
      setData();
    } else {
      history.push("/login");
    }
  }, []);

  return (
    <Container className="main">
      <div className="maintext">
        <h1>Challenges</h1>
        <p>
          Sort challenges by difficulty or major. Challenges associated with a
          specific major are themed to fit programming applications of that
          major.
        </p>
        <h4>Select a language...</h4>
        <select name="Select Language" onChange={handleLangChange} id="lang">
          <option value="java">Java</option>
          <option value="cpp">C++</option>
          <option value="c">C</option>
          <option value="python">Python</option>
        </select>

        <h4>Select a major...</h4>
        <select
          name="Select Major"
          onChange={handleMajorChange}
          // selected={selectedMajor}
          value={selectedMajor}
          id="major"
        >
          <option value="All">All</option>
          <option value="Computer Science">Computer Science/Engineering</option>
          <option value="Electrical Engineering">Electrical Engineering</option>
          <option value="...">...</option>
        </select>
      </div>

      <ListGroup className="w-100 p-3 easyChalls">
        {showEasyChallenges()}
      </ListGroup>
      <ListGroup className="w-100 p-3 medChalls">
        {showMediumChallenges()}
      </ListGroup>
      <ListGroup className="w-100 p-3 hardChalls">
        {showHardChallenges()}
      </ListGroup>
    </Container>
  );
}

export default Challenges;
