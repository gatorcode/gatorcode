import React, { useEffect, useState } from "react";
import "./styles/competition.css";
import { Container, Jumbotron, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import { API_URL } from "../constants";


function Competition(props) {
  const [userdata, setUserdata] = useState(props.data);
  const [compMap, setCompMap] = useState([]);
  const [regButtonPressed, setRegButtonPressed] = useState(false);
  const [username, setUsername] = useState("");

  
  const history = useHistory();

  const goToChallenge = async () => {
    const dataobj = await userdata;

    const title = localStorage.getItem("selectedComp");

    Axios({
      method: "POST",
      data: {
        title: title,
        username: dataobj.data.username
      },
      withCredentials: true,
      url: `${API_URL}/checkChallengeReg`
    }).then(res => {
      if (res.data === "Registered!") {
        //alert("Registered!");
        //!GO TO CHALLENGE
      } else if (res.data === "Not Registered!") {
        alert("Not Registered!");
        setRegButtonPressed(false);
        window.location.reload();
      }
    });
  };

  const registerComp = async () => {
    const dataobj = await userdata;

    const title = localStorage.getItem("selectedComp");

    Axios({
      method: "POST",
      data: {
        title: title,
        username: dataobj.data.username
      },
      withCredentials: true,
      url: `${API_URL}/registerCompetition`
    }).then(res => {
      if (res.data === "User already registered!") {
        alert("Already Registered!");
        setRegButtonPressed(false);
      } else {
        window.location.reload();
      }
    });
  };

  const getAllCompetitions = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: `${API_URL}/getallcompetitions`
    });
  };

  const handleRegPress = () => {
    setRegButtonPressed(true);
  };

  const setData = async () => {
    const data = await getAllCompetitions();
    setCompMap(data.data);
  };

  const showComps = () => {
    const comps = compMap.map((i, j) => {
      const title = compMap[j].title;
      const date = compMap[j].date;
      const regClosing = compMap[j].regClosing;
      const shortDesc = compMap[j].shortDesc;
      const firstPrize = compMap[j].firstPrize;
      const secondPrize = compMap[j].secondPrize;
      const thirdPrize = compMap[j].thirdPrize;
      const live = compMap[j].live;
      const registration_active = compMap[j].registration_active;
      const registeredUsers = compMap[j].registeredUsers;

      //stores whether or not user is already registered for the competition
      const registeredState = registeredUsers.includes(username);

      return (
        <Jumbotron>
          <Container>
            <p className="comp-title">{title}</p>
            <p>
              Date: <b>{date}</b>
            </p>
            <p>
              Registration Closing: <b>{regClosing}</b>
            </p>
            <p>{shortDesc}</p>
            <ul>
              <p>Winning Prizes:</p>
              <li>{firstPrize}</li>
              <li>{secondPrize}</li>
              <li>{thirdPrize}</li>
            </ul>

            {!live ? (
              <div className="regbuttons">
                {registration_active ? (
                  <div className="">
                    {!regButtonPressed ? (
                      <div className="">
                        {registeredState ? (
                          <Button disabled>Registered</Button>
                        ) : (
                          <Button
                            onClick={() => {
                              {
                                handleRegPress();
                              }
                              localStorage.setItem("selectedComp", title);
                            }}
                          >
                            Click to Register
                          </Button>
                        )}
                      </div>
                    ) : (
                      <Button onClick={registerComp}>Confirm?</Button>
                    )}
                  </div>
                ) : (
                  <Button disabled>Registration Closed</Button>
                )}
              </div>
            ) : (
              <div className="">
                {!regButtonPressed ? (
                  <div className="">
                    {registeredState ? (
                      <Button onClick={handleRegPress}>GO!</Button>
                    ) : (
                      <Button disabled>Not Registered!</Button>
                    )}
                  </div>
                ) : (
                  <Button href={`/competition/${title}`}>Confirm?</Button>
                )}
              </div>
            )}
          </Container>
        </Jumbotron>
      );
    });

    return comps;
  };

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(async () => {
    clearLocalStorage();
    if (localStorage.getItem("usl") === "aaa" && userdata) {
      console.log("logged in");
      const dataobj = await userdata;
      await setUsername(dataobj.data.username);
      await setData();
    } else {
      history.push("/login");
    }
  }, []);

  return (
    <div className="body-main">
      <h1>Upcoming Competitions</h1>
      <div className="comps-main">{showComps()}</div>
    </div>
  );
}

export default Competition;
