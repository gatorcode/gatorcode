import React, { useState, useEffect } from "react";
import "./styles/challenge.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import { Row, Col, Accordion, Button, Card, Container } from "react-bootstrap";
import Ide from "../components/ide";
import { useParams } from "react-router-dom";
import { API_URL } from "../constants";

const port = 8011; //compiler

function Challenge(props) {
  const [userdata, setUserdata] = useState(props.data);

  const [challengeTitle, setChallengeTitle] = useState(useParams().title);
  const [challengeData, setChallengeData] = useState({});

  //states for challenge
  const [checkingAnswer, setCheckingAnswer] = useState(true);
  const [correct, setCorrect] = useState(false);
  const [alreadyComplete, setAlreadyComplete] = useState(false);
  const [incorrect, setIncorrect] = useState(false);
  const [invalidSolution, setInvalidSolution] = useState(false);
  const [desc, setDesc] = useState("");
  const [inputType, setInputType] = useState("");

  const [testCaseResult, setTestCaseResult] = useState("");

  var userPoints = 0;
  var userLevel = 0;
  var completedChallenges = {};

  let testCaseArray = [];

  const getData = () => {
    return Axios({
      method: "post",
      url: `${API_URL}/getchallenge`,
      data: {
        title: challengeTitle
      }
    });
  };

  const completeChallenge = async () => {
    const dataobj = await userdata;

    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;

    completedChallenges = dataobj.data.challengesCompleted;

    const complete = dataobj.data.challengesCompleted[challengeTitle];

    const correctOutput = challengeData.correctOutput;
    const userOutput = document.getElementById("result").value;

    testCaseArray = [];
    //check for correct output
    if (correctOutput === userOutput) {
      //check other test cases

      let validSolution = true;

      const testCases = await challengeData.testcases;

      console.log(testCases);

      for (let i = 0; i < testCases.length; i++) {
        //console.log(testCases[i][0]);
        await submitTestCase(testCases[i][0]);
        const temp = testCaseArray;
        temp.push(testCaseResult);
        console.log(testCaseResult);
        console.log(temp);
        testCaseArray.concat(temp.push(testCaseResult));
        setTestCaseResult("");
      }

      //console.log(testCaseArray); //!CANNOT ACCESS

      for (let i = 0; i < testCases.length; i++) {
        //console.log(testCaseArray[0]);
        if (testCaseArray[i] !== testCases[i][1]) {
          validSolution = false;
        }
      }

      /*
      #include <iostream>
#include <string>
 using namespace std;     
void reverse_string(std::string argument){
  string greeting = argument;
  int len = greeting.length();
  int n=len-1;
  for(int i=0;i<(len/2);i++){
    swap(greeting[i],greeting[n]);
    n = n-1;

  }
  cout<<greeting<<endl;

}
      
int main(int argc, char* argv[]){
  std::string argument = std::string(argv[1]);
  reverse_string(argument); //your function call
  return 0;
}
      */

      //console.log(testCaseArray);

      if (validSolution) {
        //check for completion status
        if (complete === 0) {
          //add points
          setCheckingAnswer(false);
          setCorrect(true);
          setIncorrect(false);
          //alert("CORRECT!");
          switch (challengeData.difficulty) {
            case "Easy":
              userPoints += 5;
              break;
            case "Medium":
              userPoints += 10;
              break;
            case "Hard":
              userPoints += 15;
              break;
          }

          //mark as complete
          completedChallenges[challengeTitle] = 2;
          //console.log(completedChallenges);

          if (userPoints >= userLevel * 5) {
            userPoints = userPoints - userLevel * 5;
            userLevel += 1;
            addPoints();
          } else {
            addPoints();
          }
          window.location.reload();
        } else {
          setCheckingAnswer(false);
          setAlreadyComplete(true);
          setCorrect(true);
          setIncorrect(false);
        }
      } else {
        setCheckingAnswer(false);
        setIncorrect(true);
        setCorrect(false);

        //alert("INVALID SOLUTION!");
      }
    } else {
      //challenge attempted
      setCheckingAnswer(false);
      setIncorrect(true);
      setCorrect(false);

      //alert("INCORRECT!");
      if (complete !== 2) {
        completedChallenges[challengeTitle] = 1;
        addPoints();
      }
    }
  };

  const submitTestCase = async input => {
    //console.log("SUBMITTED TEST CASE");
    const lang = localStorage.getItem("lang");

    const code = localStorage.getItem("code");

    let fileName = "";
    if (localStorage.getItem("selectedtitle")) {
      //if in sandbox
      fileName = localStorage.getItem("selectedtitle");
    } else if (document.getElementById("challengeTitle")) {
      //if in challenges
      fileName = `gcode.${localStorage.getItem("lang")}`;
    } else if (document.getElementById("lessonTitle")) {
      //if in lessons

      fileName = `gcode.${localStorage.getItem("lang")}`;
    }

    Axios.post(`http://localhost:${port}/code/submit${lang}/`, {
      code: code,
      input: input,
      lang: lang,
      fileName: fileName
    }).then(res => {
      if (res.data.stderr) {
        setTestCaseResult(res.data.stderr);
      } else if (res.data.shortMessage) {
        setTestCaseResult(res.data.shortMessage);
      } else {
        setTestCaseResult(res.data);
      }
    });
  };

  const addPoints = async () => {
    const dataobj = await userdata;

    Axios({
      method: "PUT",
      data: {
        username: dataobj.data.username,
        points: userPoints,
        level: userLevel,
        challengesCompleted: completedChallenges
      },
      withCredentials: true,
      url: `http://localhost:${process.env.PORT || 5000}/addpoints`
    });
  };

  const setData = async () => {
    const data = await getData();
    const dataobj = await userdata;
    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;
    await setChallengeData(data.data);
    //await setInputType(data.data.inputType);
    setCodeTemplate(data.data.inputType);
  };

  //set input value to specified value in challengeData depending on language
  const setInput = () => {
    let input = "";

    switch (localStorage.getItem("lang")) {
      case "java":
        input = challengeData.inputjava;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "cpp":
        input = challengeData.inputcpp;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "c":
        input = challengeData.inputc;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "python":
        input = challengeData.inputpython;
        console.log(input);
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
    }
  };

  const setCodeTemplate = inputTy => {
    const input = document.getElementById("input").value;

    const funcName = `${challengeTitle.toLowerCase().replaceAll(" ", "_")}`;

    //console.log(inputTy);

    const javaTemplate = `public class myClass {

        public static void main (String[] args) {

            for (String s: args) {

                System.out.println(s);

            }

            ${funcName};

        }

    }
    
    public void ${funcName}() {

      //YOUR SOLUTION GOES HERE

    }`;

    //!C NOT CURRENTLY SUPPORTED

    var cTemplate = ``;

    if (inputTy == "Array") {
      cTemplate = `#include <stdio.h>  
#include <unistd.h>  

void ${funcName}(char *argus[]){

  //printf(argus[0])

  //for (int i = 0; i < 4; ++i)
  // {
  //  printf(argus[i]);
  //}
    

}
  
int main(int argc, char *argv[])  
{ 
    int opt; 
              
    const char *arguments[5];
  
    for(; optind < argc; optind++){      
        arguments[optind] = argv[optind];
        // printf("extra arguments: %s\n", argv[optind]);  
    } 

    ${funcName}(arguments);
    
    return 0; 
} 
      `;
    } else if (
      inputTy == "String" ||
      inputTy == "Number" ||
      inputTy == "Object"
    ) {
      cTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
         
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    } else if (inputTy == "None") {
      cTemplate = `#include <stdio.h>  
#include <unistd.h>  

void ${funcName}(){

  printf("Hello World!");

}
  
int main(int argc, char *argv[])  
{ 
    ${funcName}(); //your function call
    
    return 0; 
} 
      `;
    }

    // CPP TEMPLATE

    var cppTemplate = ``;

    if (inputTy == "Array") {
      cppTemplate = `#include <iostream>
#include <string>
#include <vector>
      
  
std::vector<std::string> parse_input(std::string args) {
  /*
  !!!DO NOT CHANGE!!!
  This function parses the input arguments and returns a vector<string> of arguments
  */
  
  std::vector<std::string> rect_vect;
  std::string s = args;
  std::string delimiter = ",";
    
  size_t pos = 0;
  std::string token;
  while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    s.erase(0, pos + delimiter.length());
    rect_vect.push_back(token);
  }
      
  return rect_vect;
  
}
      
void ${funcName}(std::vector<std::string> input) {
  /*
  YOUR SOLUTION GOES HERE
  Use '[]' to access elements in input
  i.e. input[0] will access the first element in the 'input' vector
  */
 
  std::cout << "Hello World!" << std::endl;
}
    
int main(int argc, char** argv) { 
  //stores the argumment value(s) from the 'Input' box below
  std::string input = argv[1];
    
  //passes the parsed input vector into your function as a param
  ${funcName}(parse_input(input));
    
  return 0; 

}
  `;
    } else if (
      inputTy == "String" ||
      inputTy == "Number" ||
      inputTy == "Object"
    ) {
      cppTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
  std::cout << "Hello World!" << std::endl;

}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    } else if (inputTy == "None") {
      cppTemplate = `#include <iostream>
      
void ${funcName}(){
  //YOUR CODE GOES HERE
  std::cout << "Hello World!" << std::endl;
          
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  ${funcName}(); //your function call
  return 0;
}`;
    }

    // PYTHON TEMPLATE

    var pythonTemplate = ``;
    if (inputTy !== "None") {
      pythonTemplate = `import sys

#DO NOT CHANGE
#parse arguments and returns an array of *strings*
def parse_input():
  return sys.argv[1].split(',')
  
def ${funcName}(parsed_input): 
  #YOUR SOLUTION GOES HERE
  print(parsed_input[0])

#function call with input as param
${funcName}(parse_input())
`;
    } else {
      pythonTemplate = `import sys
  
def ${funcName}(): 
  #YOUR SOLUTION GOES HERE
  print("Hello World!")

#function call
${funcName}()
`;
    }

    switch (localStorage.getItem("lang")) {
      case "java":
        document.getElementById("code").value = javaTemplate;
        break;
      case "python":
        document.getElementById("code").value = pythonTemplate;
        break;
      case "cpp":
        document.getElementById("code").value = cppTemplate;
        break;
      case "c":
        document.getElementById("code").value = cTemplate;
        break;
    }
  };

  //parse input type
  const handleInputType = () => {};

  const hintLangHandler = () => {
    switch (localStorage.getItem("lang")) {
      case "java":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1java}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2java}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3java}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "python":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1python}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2python}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3python}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "c":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1c}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2c}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3c}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
      case "cpp":
        return (
          <Accordion>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="0">
                  Hint 1
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="0">
                <Card.Body>{challengeData.hint1cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="1">
                  Hint 2
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="1">
                <Card.Body>{challengeData.hint2cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Card.Header>
                <Accordion.Toggle as={Button} variant="link" eventKey="3">
                  Hint 3
                </Accordion.Toggle>
              </Card.Header>
              <Accordion.Collapse eventKey="3">
                <Card.Body>{challengeData.hint3cpp}</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        );
    }
  };

  const renderStatus = () => {
    if (correct) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <polyline
            class="path check"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            points="100.2,40.2 51.5,88.8 29.8,67.5 "
          />
        </svg>
      );
    } else if (incorrect) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="34.4"
            y1="37.9"
            x2="95.8"
            y2="92.3"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="95.8"
            y1="38"
            x2="34.4"
            y2="92.2"
          />
        </svg>
      );
    } else {
      return <div className="loader"></div>;
    }
  };

  // const setDescMethod = async () => {
  //   await challengeData;
  //   setDesc(challengeData.desc.replace(/\\n/g, "<br>\n"));
  // };

  const fillData = () => {
    //setDescMethod();
    return (
      <Card style={{ width: "35rem" }}>
        <Card.Body>
          <Card.Title id="challengeTitle">{challengeData.title}</Card.Title>

          <Card.Body className="mb-2 text-muted">
            <p>
              <b>Introduction:</b>
            </p>
            {challengeData.intro}
          </Card.Body>
          <Card.Body className="mb-2 text-muted">
            <p>
              <b>Description:</b>
            </p>
            {challengeData.desc}
            {challengeData.figure ? (
              <Card.Body className="mb-2 text-muted">
                <img id="figure" src={challengeData.figure} alt="figure" />
              </Card.Body>
            ) : null}
          </Card.Body>
          {challengeData.example1 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 1: </b>
              </p>
              {challengeData.example1}
            </Card.Body>
          ) : null}
          {challengeData.example2 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 2: </b>
              </p>
              {challengeData.example2}
            </Card.Body>
          ) : null}
          {challengeData.example3 ? (
            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Example 3: </b>
              </p>
              {challengeData.example3}
            </Card.Body>
          ) : null}

          <div className="inputoutput">
            {challengeData.inputjava ? (
              <Card.Body className="mb-2 text-muted">
                <p>
                  <b>Input: </b>
                </p>
                {challengeData.inputpython}
              </Card.Body>
            ) : null}

            <Card.Body className="mb-2 text-muted">
              <p>
                <b>Expected Output: </b>
              </p>
              {challengeData.correctOutput}
            </Card.Body>
            {renderSetInputButton()}
          </div>
        </Card.Body>
        {hintLangHandler()}
      </Card>
    );
  };

  const renderSetInputButton = () => {
    if (challengeData.inputjava) {
      return <Button onClick={setInput}>Set Example Input</Button>;
    } else {
      return (
        <Button id="setInputButton" disabled onClick={setInput}>
          Set Test Input
        </Button>
      );
    }
  };

  useEffect(() => {
    setData();
  }, []);

  return (
    <Row>
      <div className="challengeInfo">
        <Col>
          {fillData()}
          <Container>
            <Button id="checkAnswer" onClick={() => completeChallenge()}>
              Check Answer
            </Button>
            {renderStatus()}
          </Container>
        </Col>
      </div>
      <div className="sandboxide">
        <Col>
          <Ide />
          <Row></Row>
        </Col>
      </div>
    </Row>
  );
}

export default Challenge;
