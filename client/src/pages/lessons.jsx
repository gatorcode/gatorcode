import React, { useState, useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import "./styles/challenges.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, ListGroup, Accordion, Card } from "react-bootstrap";
import Axios from "axios";
import { API_URL } from "../constants";

function Lessons(props) {
  const history = useHistory();

  //const [userdata, setUserdata] = useState(props.data);

  const [lessonMap, setLessonMap] = useState([]);

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(() => {
    clearLocalStorage();
  });

  const getAllLessons = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: `${API_URL}/getalllessons`
    });
  };

  const setData = async () => {
    const data = await getAllLessons();
    setLessonMap(data.data);
  };

  const showLessonOutputsAndVariables = () => {
    return lessonMap.map((i, j) => {
      const section = lessonMap[j].section;
      if (section == "Outputs and Variables") {
        return (
          <React.Fragment>
            <ListGroup.Item
              className="lessonCard"
              action
              href={`/lessonPages/${lessonMap[j].title}`}
              variant="success"
            >
              {lessonMap[j].title}
            </ListGroup.Item>
          </React.Fragment>
        );
      }
    });
  };

  const showLesson2 = () => {
    return lessonMap.map((i, j) => {
      const section = lessonMap[j].section;
      if (section === "2") {
        return (
          <React.Fragment>
            <ListGroup.Item
              className="lessonCard"
              action
              href={`/lessonPages/${lessonMap[j].title}`}
              variant="success"
            >
              {lessonMap[j].title}
            </ListGroup.Item>
          </React.Fragment>
        );
      }
    });
  };

  const handleLangChange = e => {
    const langVal = e.target.value;
    localStorage.setItem("lang", langVal);
  };

  useEffect(async () => {
    if (localStorage.getItem("usl") === "aaa") {
      setData();
    } else {
      history.push("/login");
    }
  }, []);
  /*
  return (
    <Accordion>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="0">
          Lesson 1: Inputs and Outputs
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            <Link to="/lessonPages"> 1.1 "Hello World"</Link>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="1">
          Lesson 2: ----
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1">
          <Card.Body>2.1 "---"</Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
}
*/

  //<option value="c">C</option>
  // C has been removed under select language
  return (
    <Container className="main">
      <div className="maintext">
        <h1>Lessons</h1>
        <p>
          Lessons are associted with a specific coding language and should be
          completed chronologically.
        </p>
      </div>

      <select name="Select Language" onChange={handleLangChange} id="lang">
        <option value="java">Java</option>
        <option value="cpp">C++</option>
        <option value="python">Python</option>
      </select>
      <h1></h1>
      <Accordion>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">
            Lesson 1: Outputs and Variables
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <ListGroup className="w-100 p-3 lessons">
                {showLessonOutputsAndVariables()}
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
          <Accordion.Toggle as={Card.Header} eventKey="1">
            Lesson 2: ...
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <ListGroup className="w-100 p-3 lessons">
                {showLesson2()}
              </ListGroup>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </Container>
  );
}

export default Lessons;
