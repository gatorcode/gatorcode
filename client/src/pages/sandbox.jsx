/*
----------NOTES----------
*/

import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { useHistory } from "react-router-dom";
import { API_URL } from "../constants";
import { Row, Col, Button, Modal } from "react-bootstrap";
import "./styles/sandbox.css";
import Axios from "axios";
import Ide from "../components/ide";

function Sandbox(props) {
  const [userdata, setUserdata] = useState(props.data);

  const [sandboxes, setSandboxes] = useState([]);
  const [lang, setLang] = useState("");
  const [sandname, setSandname] = useState("");

  const [curCode, setCurCode] = useState("");

  const [showmodal, setShowmodal] = useState(false);

  const history = useHistory();

  //state for name of current sandBox
  const [name, setName] = useState(
    localStorage.getItem("name") ? localStorage.getItem("name") : ""
  );

  //state for saved sandbox
  const [saved, setSaved] = useState(false);

  const handleClose = () => {
    setShowmodal(false);
  };

  const handleShow = () => {
    if (sandboxes.length <= 4) {
      setShowmodal(true);
    } else {
      alert("Too many sandboxes!");
    }
  };

  const changeIDECode = () => {
    if (localStorage.getItem("0")) {
      document.getElementById("sand0").onclick = () => {
        setCurCode(localStorage.getItem("0"));
        localStorage.setItem("selected", `${localStorage.getItem("0")}`);
        localStorage.setItem(
          "selectedtitle",
          `${localStorage.getItem("0title")}`
        );
        localStorage.setItem("name", `${localStorage.getItem("0title")}`);

        if (localStorage.getItem("0title").endsWith("a")) {
          setLang("java");
          localStorage.setItem("lang", "java");
        } else if (localStorage.getItem("0title").endsWith("p")) {
          setLang("cpp");
          localStorage.setItem("lang", "cpp");
        } else if (localStorage.getItem("0title").endsWith("c")) {
          setLang("c");
          localStorage.setItem("lang", "c");
        } else if (localStorage.getItem("0title").endsWith("y")) {
          setLang("python");
          localStorage.setItem("lang", "python");
        }
      };
    }
    if (localStorage.getItem("1")) {
      document.getElementById("sand1").onclick = () => {
        setCurCode(localStorage.getItem("1"));
        localStorage.setItem("selected", `${localStorage.getItem("1")}`);
        localStorage.setItem(
          "selectedtitle",
          `${localStorage.getItem("1title")}`
        );

        localStorage.setItem("name", `${localStorage.getItem("1title")}`);

        if (localStorage.getItem("1title").endsWith("a")) {
          setLang("java");
          localStorage.setItem("lang", "java");
        } else if (localStorage.getItem("1title").endsWith("p")) {
          setLang("cpp");
          localStorage.setItem("lang", "cpp");
        } else if (localStorage.getItem("1title").endsWith("c")) {
          setLang("c");
          localStorage.setItem("lang", "c");
        } else if (localStorage.getItem("1title").endsWith("y")) {
          setLang("python");
          localStorage.setItem("lang", "python");
        }
      };
    }
    if (localStorage.getItem("2")) {
      document.getElementById("sand2").onclick = () => {
        setCurCode(localStorage.getItem("2"));
        localStorage.setItem("selected", `${localStorage.getItem("2")}`);
        localStorage.setItem(
          "selectedtitle",
          `${localStorage.getItem("2title")}`
        );

        localStorage.setItem("name", `${localStorage.getItem("2title")}`);

        if (localStorage.getItem("2title").endsWith("a")) {
          setLang("java");
        } else if (localStorage.getItem("2title").endsWith("p")) {
          setLang("cpp");
        } else if (localStorage.getItem("2title").endsWith("c")) {
          setLang("c");
        } else if (localStorage.getItem("2title").endsWith("y")) {
          setLang("python");
        }
      };
    }
    if (localStorage.getItem("3")) {
      document.getElementById("sand3").onclick = () => {
        setCurCode(localStorage.getItem("3"));
        localStorage.setItem("selected", `${localStorage.getItem("3")}`);
        localStorage.setItem(
          "selectedtitle",
          `${localStorage.getItem("3title")}`
        );

        localStorage.setItem("name", `${localStorage.getItem("3title")}`);

        if (localStorage.getItem("3title").endsWith("a")) {
          setLang("java");
          localStorage.setItem("lang", "java");
        } else if (localStorage.getItem("3title").endsWith("p")) {
          setLang("cpp");
          localStorage.setItem("lang", "cpp");
        } else if (localStorage.getItem("3title").endsWith("c")) {
          setLang("c");
          localStorage.setItem("lang", "c");
        } else if (localStorage.getItem("3title").endsWith("y")) {
          setLang("python");
          localStorage.setItem("lang", "python");
        }
      };
    }
    if (localStorage.getItem("4")) {
      document.getElementById("sand4").onclick = () => {
        setCurCode(localStorage.getItem("4"));
        localStorage.setItem(
          "selectedtitle",
          `${localStorage.getItem("4title")}`
        );
        localStorage.setItem("selected", `${localStorage.getItem("4")}`);
        localStorage.setItem("name", `${localStorage.getItem("4title")}`);

        if (localStorage.getItem("4title").endsWith("a")) {
          setLang("java");
          localStorage.setItem("lang", "java");
        } else if (localStorage.getItem("4title").endsWith("p")) {
          setLang("cpp");
          localStorage.setItem("lang", "cpp");
        } else if (localStorage.getItem("4title").endsWith("c")) {
          setLang("c");
          localStorage.setItem("lang", "c");
        } else if (localStorage.getItem("4title").endsWith("y")) {
          setLang("python");
          localStorage.setItem("lang", "python");
        }
      };
    }
  };

  const getSandboxes = async () => {
    const dataobj = await userdata;
    const sb = dataobj.data.sandboxes;
    if (sb && localStorage.getItem("usl") == "aaa") {
      const entries = Object.entries(sb);
      setSandboxes(entries);
    }
  };

  const retItems = () => {
    const titles = sandboxes.map((i, j) => {
      localStorage.setItem(`${j}`, `${i[1]}`);
      localStorage.setItem(`${j}title`, `${i[0]}`);

      return (
        <ListGroup.Item>
          <Row>
            <p
              id={`sand${j}`}
              className="existingsandboxes"
              onClick={changeIDECode}
            >
              {i[0]}
            </p>
            {i[0] === localStorage.getItem("selectedtitle") ? (
              <div className="">
                <p className="selectedText">[Selected]</p>
                <div id="saveIndicator">
                  <Row className="">
                    {saved ? (
                      <React.Fragment>
                        {/* <p className="">Saved:</p> */}
                        <svg id="check" viewBox="0 0 130.2 130.2">
                          <circle
                            class="path circle"
                            fill="none"
                            stroke="#73AF55"
                            stroke-width="7"
                            stroke-miterlimit="10"
                            cx="65.1"
                            cy="65.1"
                            r="62.1"
                          />
                          <polyline
                            class="path check"
                            fill="none"
                            stroke="#73AF55"
                            stroke-width="7"
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            points="100.2,40.2 51.5,88.8 29.8,67.5 "
                          />
                        </svg>
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        {/* <p className="">Saved:</p> */}
                        <svg id="check" viewBox="0 0 130.2 130.2">
                          <circle
                            class="path circle"
                            fill="none"
                            stroke="#808080"
                            stroke-width="7"
                            stroke-miterlimit="10"
                            cx="65.1"
                            cy="65.1"
                            r="62.1"
                          />
                          <polyline
                            class="path check"
                            fill="none"
                            stroke="#808080"
                            stroke-width="7"
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            points="100.2,40.2 51.5,88.8 29.8,67.5 "
                          />
                        </svg>
                      </React.Fragment>
                    )}
                  </Row>
                </div>
              </div>
            ) : null}
          </Row>
        </ListGroup.Item>
      );
    });
    return titles;
  };

  const addSandbox = async () => {
    const dataobj = await userdata;

    const sb = dataobj.data.sandboxes;

    const code = localStorage.getItem("code");

    const entries = Object.entries(sb);

    if (sandboxes.length <= 5) {
      let suffix = "";
      let template = "";
      switch (localStorage.getItem("lang")) {
        case "python":
          suffix = "py";
          template = `import sys

print("Argument List:", str(sys.argv))`;
          break;
        case "cpp":
          suffix = "cpp";
          template = `#include <iostream> 
      
int main(int argc, char** argv){
return 0; 
}`;
          break;
        case "c":
          suffix = "c";
          template = `#include<stdio.h> 

int main(int argc,char* argv[]){
return 0; 
}`;
          break;
        case "java":
          suffix = "java";
          template = `public class ClassName {

  public static void main (String[] args) {
    
    for (String s: args) {
    
      System.out.println(s);
    
    }
    
  }
    
}`;
          break;
      }

      const newobj = [[`${localStorage.getItem("name")}.${suffix}`, template]];

      const finaldata = newobj.concat(entries);

      const newsand = Object.fromEntries(finaldata);

      console.log(dataobj.data.username);

      Axios({
        method: "PUT",
        data: {
          username: dataobj.data.username,
          sandboxes: newsand
        },
        withCredentials: true,
        url: `${API_URL}/newsandbox`
      });
    } else {
      alert("Too many sandboxes!");
    }
  };

  const deleteSandbox = async () => {
    //gets all user data
    const dataobj = await userdata;

    //gets the sanboxes from the user schema
    const sb = dataobj.data.sandboxes;

    //converts object to an array and stores in 'entries'
    const entries = Object.entries(sb);

    //console.log(entries);

    //get name of sandbox to delete
    const toDelete = localStorage.getItem("selectedtitle");

    //remove from entries
    entries.map((i, j) => {
      if (entries[j][0] === toDelete) {
        entries.splice(j, 1);
      }
    });

    //store in new objec
    const newsand = entries;

    //convert to object
    const newsandObj = Object.fromEntries(newsand);

    Axios({
      method: "PUT",
      data: {
        username: dataobj.data.username,
        sandboxes: newsandObj
      },
      withCredentials: true,
      url: `${API_URL}/deletesandbox`
    });

    clearLocalStorage();
    window.location.reload();
  };

  const updateSandbox = async () => {
    //sets saved state to true
    setTimeout(() => {
      setSaved(true);
    }, 5000);

    //gets all user data
    const dataobj = await userdata;

    //gets the sanboxes from the user schema
    const sb = dataobj.data.sandboxes;

    //converts object to an array and stores in 'entries'
    const entries = Object.entries(sb);

    //get name of sandbox to update
    const toUpdate = localStorage.getItem("selectedtitle");

    //remove from entries
    entries.map((i, j) => {
      if (entries[j][0] === toUpdate) {
        entries[j][1] = localStorage.getItem("code");
      }
    });

    //store in new objec
    const newsand = entries;

    //convert to object
    const newsandObj = Object.fromEntries(newsand);

    Axios({
      method: "PUT",
      data: {
        username: dataobj.data.username,
        sandboxes: newsandObj
      },
      withCredentials: true,
      url: `${API_URL}/updatesandbox`
    });

    //alert(`${localStorage.getItem("selectedtitle")} saved!`);
  };

  useEffect(() => {
    //clearLocalStorage();
    if (localStorage.getItem("usl") === "aaa" && userdata) {
      getSandboxes();
    } else {
      history.push("/login");
    }
  }, []);

  const pressPython = e => {
    handleShow();
    setLang("python");
    localStorage.setItem("lang", "python");
  };
  const pressjava = e => {
    handleShow();
    setLang("java");
    localStorage.setItem("lang", "java");
  };
  const pressc = e => {
    handleShow();
    setLang("c");
    localStorage.setItem("lang", "c");
  };
  const presscpp = e => {
    handleShow();
    setLang("cpp");
    localStorage.setItem("lang", "cpp");
  };

  const handleSandnameChange = e => {
    setSandname(e.target.value);
  };

  const handleSaveSand = () => {
    var letters = /^[0-9a-zA-Z]+$/;

    if (userdata && localStorage.getItem("usl") === "aaa") {
      if (sandname.match(letters)) {
        addSandbox();
        localStorage.setItem("name", sandname);
        handleClose();
        window.location.reload();
      } else {
        alert("Please enter alphanumeric characters only!");
      }
    }
  };

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  const reloadPage = () => {
    window.location.reload();
  };

  const loadCurrentSandbox = async () => {
    await changeIDECode();
    await reloadPage();
  };

  document.onkeypress = function(key_dtl) {
    if (localStorage.getItem("code")) {
      updateSandbox();
      setSaved(false);
    }
  };

  //save data after time
  //get code after clicking on name
  //delete

  return (
    <div className="main">
      <Modal show={showmodal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New {lang} sandbox</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Enter name of new {lang} sandbox! (Language suffix will be added
          automatically)
        </Modal.Body>
        <input maxLength="12" onChange={handleSandnameChange} />

        <Modal.Footer>
          <Button variant="primary" onClick={handleSaveSand}>
            Create
          </Button>
        </Modal.Footer>
      </Modal>

      <Row>
        <div id="sandselect" className="sandselec">
          <Col>
            <Card className="chooseLang" style={{ width: "35rem" }}>
              <Card.Body>
                <Card.Title>Create New Sandbox </Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                  Select Language
                </Card.Subtitle>
                <div className="langbuttons">
                  <Button className="langbutton" onClick={pressPython}>
                    Python
                  </Button>
                  {/* <Button className="langbutton" onClick={pressc}>
                    C
                  </Button> */}
                  <Button className="langbutton" onClick={presscpp}>
                    C++
                  </Button>
                  <Button className="langbutton" onClick={pressjava}>
                    Java
                  </Button>
                </div>
              </Card.Body>
            </Card>
            <Card style={{ width: "35rem" }}>
              <Card.Body>
                <Card.Title>Your Sandboxes</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">
                  (Max: 5)
                </Card.Subtitle>
                <ListGroup variant="flush">{retItems()}</ListGroup>
                <ListGroup>
                  <Button
                    className="sandOptions"
                    variant="success"
                    onClick={loadCurrentSandbox}
                  >
                    Load Sandbox
                  </Button>

                  <Button
                    className="sandOptions"
                    variant="danger"
                    id="delete-sandbox"
                    onClick={deleteSandbox}
                  >
                    Delete
                  </Button>
                </ListGroup>
              </Card.Body>
            </Card>
          </Col>
        </div>
        <div className="sandboxide">
          <Col>
            {/* <Row>
              <h1 className="maintext" id="curSandName">
                {name}
              </h1>
            </Row> */}

            <Ide {...props} data={userdata} />
          </Col>
        </div>
      </Row>
    </div>
  );
}

export default Sandbox;
