import React, { useState } from "react";
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Button, Container, Jumbotron, Col } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { API_URL } from "../constants";
//import mainLogo from "../media/mainLogo.png";

function Register() {
  const [registerUsername, setRegisterUsername] = useState("");
  const [registerPassword, setRegisterPassword] = useState("");
  const [regfirstname, setregfirstname] = useState("");
  const [reglastname, setreglastname] = useState("");
  const [regufemail, setregufemail] = useState("");
  const [reglevel, setreglevel] = useState(1);
  const [regpoints, setregpoints] = useState(1);
  const [regavatar, setregavatar] = useState(1);
  const [regclass, setregclass] = useState([]);
  const [regsandboxes, setregsandboxes] = useState({
    WELCOME:
      "To create a new sandbox, select a language, enter a name (language suffix will be added automatically), and press 'Create'."
  });
  const [regmajor, setregmajor] = useState("");
  const [regyear, setregyear] = useState("");
  const [regLessonsCompleted, setRegLessonsCompleted] = useState({
    "Hello World": 0
  });
  const [regChallengesCompleted, setRegChallengesCompleted] = useState({
    "Hello World": 0,
    "Recursive Fibonacci": 0,
    "Grade Calculator": 0,
    "Reverse String": 0,
    "Caesar's Cipher": 0,
    "Kirchhoff Voltage Law": 0
  });

  const history = useHistory();

  const registerUser = () => {
    Axios({
      method: "POST",
      data: {
        username: registerUsername,
        password: registerPassword,
        firstname: regfirstname,
        lastname: reglastname,
        ufemail: regufemail,
        level: reglevel,
        points: regpoints,
        avatar: regavatar,
        major: regmajor,
        year: regyear,
        class: regclass,
        sandboxes: regsandboxes,
        lessonsCompleted: regLessonsCompleted,
        challengesCompleted: regChallengesCompleted
      },
      withCredentials: true,
      url: `${API_URL}/register`
    })
      .then(res => console.log(res))
      .then(history.push("/"));
  };

  return (
    <React.StrictMode>
      <Jumbotron className="headerReg">
        <h1 id="titleh1">GatorCode</h1>
        <p>Code the Gator way.</p>
      </Jumbotron>
      <Container>
        <Form.Row>
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Username"
              name=""
              id=""
              onChange={e => setRegisterUsername(e.target.value)}
            />
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              name=""
              id=""
              onChange={e => setRegisterPassword(e.target.value)}
            />
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} controlId="formGridFirstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="First name"
              name=""
              id=""
              onChange={e => setregfirstname(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formGridLastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Last name"
              name=""
              id=""
              onChange={e => setreglastname(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="formGridEmail">
            <Form.Label>UF Email</Form.Label>
            <Form.Control
              type="text"
              placeholder="UF Email"
              name=""
              id=""
              onChange={e => setregufemail(e.target.value)}
            />
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} controlId="formGridMajor">
            <Form.Label>Major</Form.Label>
            <Form.Control
              type="text"
              as="select"
              defaultValue="Choose..."
              onChange={e => setregmajor(e.target.value)}
            >
              <option>Choose...</option>
              <option>Computer Science</option>
              <option>Computer Engineering</option>
              <option>Electrical Engineering</option>
              <option>FILL IN REST</option>
            </Form.Control>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridYear">
            <Form.Label>Year</Form.Label>
            <Form.Control
              type="text"
              as="select"
              defaultValue="Choose..."
              onChange={e => setregyear(e.target.value)}
            >
              <option>Choose...</option>
              <option>Freshman</option>
              <option>Sophomore</option>
              <option>Junior</option>
              <option>Senior</option>
              <option>Graduate</option>
            </Form.Control>
          </Form.Group>

          <Form.Group as={Col} controlId="formGridAvatar">
            <Form.Label>Avatar</Form.Label>
            <Form.Control
              as="select"
              defaultValue="Choose..."
              onChange={e => setregavatar(e.target.value)}
            >
              <option>Choose...</option>
              <option>1</option>
              <option>2</option>
            </Form.Control>
          </Form.Group>
        </Form.Row>

        <Button variant="primary" type="submit" onClick={registerUser}>
          Submit
        </Button>
      </Container>
    </React.StrictMode>
  );
}

export default Register;
