import React, { useEffect } from "react";
import "./styles/homepage.css";
import Container from "react-bootstrap/Container";
import gatorlogo from "../media/mainLogo.png";
import "bootstrap/dist/css/bootstrap.min.css";

function Homepage() {
  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(() => {
    clearLocalStorage();
    // if (!document.getElementById("userfirstname")) {
    //   localStorage.setItem("usl", "bbb");
    // }
  });

  return (
    <Container>
      <div className="maintext main-content row align-items-center my-5">
        <div className="col-lg-7">
          <img
            className="img-fluid rounded mb-4 mb-lg-0"
            src={gatorlogo}
            alt="Logo"
          />
        </div>
        <div className="col-lg-5">
          <h1 className="font-weight-light">Get started with GatorCode!</h1>
          <p>
            GatorCode is a platform for University of Florida students to learn
            and practice their coding skills. Apart from companies in the
            technology sector, the number of businesses relying on computer code
            is increasing drastically. Due to this, programming has become a
            favorable skill in almost any profession. GatorCode gives all UF
            students the opportunity to practice coding in Java, Python, and C++
            with interactive lessons and challenges. Also, create your own
            "Sandboxes" where you have the ability to get creative with your
            coding skills. Compete with your fellow Gators by earning points
            through completeing lessons, challenges, and participating in
            competitions.
          </p>
          {localStorage.getItem("usl") === "bbb" ? (
            <a className="btn btn-primary" href="/register">
              Register Now!
            </a>
          ) : null}
        </div>
      </div>

      <h2 className="maintext" id="what-can-you-do-label">
        What can you do with GatorCode?
      </h2>
      <div className="cards row">
        <div className="col-md-4 mb-5">
          <div className="card h-100">
            <div className="card-body">
              <h2 className="card-title">Lessons</h2>
              <p className="card-text">
                <ul>
                  <li>Complete lessons to level-up.</li>
                  <li>Develop or further your coding skills.</li>
                  <li>Work in Java, Python, and C++.</li>
                </ul>
              </p>
            </div>
            <div className="card-footer">
              <a href="lessons" className="btn btn-primary btn-sm">
                Go to Lessons!
              </a>
            </div>
          </div>
        </div>
        <div className="col-md-4 mb-5">
          <div className="card h-100">
            <div className="card-body">
              <h2 className="card-title">Challenges</h2>
              <p className="card-text">
                <ul>
                  <li>Level-up by completing challenges.</li>
                  <li>Prepare for technical interviews.</li>
                  <li>Major themed challenges.</li>
                  <li>Practice in Java, Python, C++.</li>
                </ul>
              </p>
            </div>
            <div className="card-footer">
              <a href="/challenges" className="btn btn-primary btn-sm">
                Go to Challenges!
              </a>
            </div>
          </div>
        </div>
        <div className="col-md-4 mb-5">
          <div className="card h-100">
            <div className="card-body">
              <h2 className="card-title">Sandbox</h2>
              <p className="card-text">
                <ul>
                  <li>
                    Create your own personal projects in Python, Java, and C++.
                  </li>
                  <li>Get creative with your coding skills.</li>
                  <li>Save up to 5 sandboxes to come back to anytime.</li>
                  <li>Sandboxes automatically save.</li>
                </ul>
              </p>
            </div>
            <div className="card-footer">
              <a href="/sandbox" className="btn btn-primary btn-sm">
                Go to your Sandbox!
              </a>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default Homepage;
