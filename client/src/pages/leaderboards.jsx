/*
NOTES
*Badges

*/
import React, { useState, useEffect } from "react";
import Axios from "axios";
import { useHistory } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Container } from "react-bootstrap";
import "./styles/leaderboards.css";
import { API_URL } from "../constants";

function Leaderboards() {
  const [userMap, setUserMap] = useState([]);
  const [selectedMajor, setSelectedMajor] = useState("Computer Science");
  const [selectedYear, setSelectedYear] = useState("");
  const [selectedLeaderboard, setSelectedLeaderboard] = useState("The Swamp");
  const history = useHistory();

  const getAllUserData = () => {
    return Axios({
      method: "GET",
      withCredentials: true,
      url: `${API_URL}/getallusers`
    });
  };

  const setData = async () => {
    const userMap = await getAllUserData();
    setUserMap(userMap.data);
  };

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(() => {
    if (localStorage.getItem("usl") === "aaa") {
      clearLocalStorage();
      setData();
    } else {
      history.push("/login");
    }
  }, []);

  const showSwampLeaderboard = () => {
    //loop through each element in the userMap array of objects
    const sortedUserMapArray = userMap.sort((a, b) => {
      return b.level - a.level;
    });

    return sortedUserMapArray.map((i, j) => {
      return (
        <React.Fragment>
          <tr>
            <td>{sortedUserMapArray[j].level}</td>
            <td>{sortedUserMapArray[j].username}</td>
            <td>{sortedUserMapArray[j].major}</td>
            <td>{sortedUserMapArray[j].year}</td>
          </tr>
        </React.Fragment>
      );
    });
  };

  const showMajorLeaderboard = () => {
    const sortedUserMapArray = userMap.sort((a, b) => {
      return b.level - a.level;
    });

    return sortedUserMapArray.map((i, j) => {
      if (sortedUserMapArray[j].major === selectedMajor) {
        return (
          <React.Fragment>
            <tr>
              <td>{sortedUserMapArray[j].level}</td>
              <td>{sortedUserMapArray[j].username}</td>
              <td>{sortedUserMapArray[j].major}</td>
              <td>{sortedUserMapArray[j].year}</td>
            </tr>
          </React.Fragment>
        );
      }
    });
  };

  const showYearLeaderboard = () => {
    //loop through each element in the userMap array of objects
    const sortedUserMapArray = userMap.sort((a, b) => {
      return b.level - a.level;
    });

    return sortedUserMapArray.map((i, j) => {
      if (sortedUserMapArray[j].year === selectedYear) {
        return (
          <React.Fragment>
            <tr>
              <td>{sortedUserMapArray[j].level}</td>
              <td>{sortedUserMapArray[j].username}</td>
              <td>{sortedUserMapArray[j].major}</td>
              <td>{sortedUserMapArray[j].year}</td>
            </tr>
          </React.Fragment>
        );
      }
    });
  };

  const showSelectedLeaderboard = () => {
    switch (selectedLeaderboard) {
      case "The Swamp":
        return showSwampLeaderboard();
      case "Year":
        return showYearLeaderboard();
      case "Major":
        return showMajorLeaderboard();
    }
  };

  const handleOptionPress = e => {
    console.log(e.target.value);
    setSelectedLeaderboard(e.target.value);
  };

  const handleYearOptionPress = async e => {
    var e = document.getElementById("yearSelect");
    await setSelectedYear(e.value);
    console.log(e.value);
    await setSelectedLeaderboard("Year");
    await showYearLeaderboard();
    var majorSelect = document.getElementById("majorSelect");
    majorSelect.value = "No Selection";
  };

  const handleMajorOptionPress = async e => {
    var e = document.getElementById("majorSelect");
    await setSelectedMajor(e.value);
    console.log(e.value);
    await setSelectedLeaderboard("Major");
    await showMajorLeaderboard();
    var yearSelect = document.getElementById("yearSelect");
    yearSelect.value = "No Selection";
  };

  return (
    //
    <body>
      <React.StrictMode>
        <div>
          <Container>
            <div className="button-group" role="group" aria-label="...">
              <Button
                onClick={handleOptionPress}
                type="button"
                className="swamp-btn"
                value="The Swamp"
              >
                The Swamp
              </Button>

              <select
                id="majorSelect"
                drop="down"
                title="Major"
                onChange={handleMajorOptionPress}
              >
                <option>No Selection</option>
                <option>Computer Science</option>
                <option>Computer Engineering</option>
              </select>
              <select
                id="yearSelect"
                drop="down"
                title="Year"
                onChange={handleYearOptionPress}
              >
                <option>No Selection</option>
                <option>Freshman</option>
                <option>Sophomore</option>
                <option>Junior</option>
                <option>Senior</option>
                <option>Graduate</option>
              </select>
            </div>
          </Container>

          <Container>
            <table class="table table-hover table-dark table-striped">
              <thead>
                <tr>
                  <th scope="col">Level</th>
                  <th scope="col">Username</th>
                  <th scope="col">Major</th>
                  <th scope="col">Year</th>
                </tr>
              </thead>
              <tbody>{showSelectedLeaderboard()}</tbody>
            </table>
          </Container>
        </div>
      </React.StrictMode>
    </body>
  );
}

export default Leaderboards;
