import React, { useEffect, useState } from "react";
import "./styles/competition-live.css";
import { Container, Button, Row, Col } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import Ide from "../components/ide";
import { API_URL } from "../constants";


function CompetitionLive(props) {
  const [userdata, setUserdata] = useState(props.data);
  const [compMap, setCompMap] = useState([]);
  const [regButtonPressed, setRegButtonPressed] = useState(false);
  const [username, setUsername] = useState("");
  const [compTitle, setCompTitle] = useState(useParams().title);

  const history = useHistory();

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  useEffect(async () => {
    clearLocalStorage();
    if (localStorage.getItem("usl") === "aaa" && userdata) {
      const dataobj = await userdata;
      await setData();
      //await setData();
    } else {
      history.push("/login");
    }
  }, []);

  const completeCompetition = () => {};

  const getData = () => {
    return Axios({
      method: "post",
      url: `${API_URL}/getcompetition`,
      data: {
        title: compTitle
      }
    });
  };

  const setData = async () => {
    const data = await getData();
    //console.log(data);
    //const dataobj = await userdata;
    await setCompMap(data.data);
  };

  const fillData = () => {
    console.log(compMap);
    return (
      <div className="">
        <p>{compTitle}</p>
        <p>{compMap.desc}</p>
      </div>
    );
  };

  const renderStatus = () => {};

  return (
    <div className="body-main">
      <Row>
        <div className="compInfo">
          <Col>
            {fillData()}
            <Container>
              <Button id="checkAnswer" onClick={() => completeCompetition()}>
                Check Answer
              </Button>
              {renderStatus()}
            </Container>
          </Col>
        </div>
        <div className="sandboxide">
          <Col>
            <Ide />
          </Col>
        </div>
      </Row>
    </div>
  );
}

export default CompetitionLive;
