import React, { useState } from "react";
import "./styles/contact.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { API_URL } from "../constants";


function Contact() {
  //-----states-----//
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");


  //-----handler functions-----//
  const setNameHandler = event => {
    setName(event.target.value);
  };

  const setEmailHandler = event => {
    setEmail(event.target.value);
  };

  const setMessageHandler = event => {
    setMessage(event.target.value);
  };


  //-----message submit function-----//
  const submitMessage = e => {
    e.preventDefault();
    console.log("hello");
    axios({
      method: "POST",
      url: `${API_URL}/sendmessage`,
      data: { name: name, email: email, message: message }
    }).then(response => {
      if (response.data.status === "success") {
        alert("Message Sent.");
        resetForm();
      } else if (response.data.status === "fail") {
        alert("Message failed to send.");
      }
    });
  };

  const resetForm = () => {
    setName("");
    setEmail("");
    setMessage("");
  };

  return (
    <div className="">
      <div className="contact-form-wrapper">
        <form className="form">
          <div className="pageTitle title">Contact Form </div>
          <div className="secondaryTitle title">
            Please fill this form to submit a message!
          </div>
          <input
            type="text"
            value={name}
            onChange={setNameHandler}
            className="name formEntry"
            placeholder="Name"
          />
          <input
            type="text"
            value={email}
            onChange={setEmailHandler}
            className="email formEntry"
            placeholder="Email"
          />
          <textarea
            value={message}
            onChange={setMessageHandler}
            className="message formEntry"
            placeholder="Message"
          ></textarea>
          <button className="submit formEntry" onClick={submitMessage}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default Contact;
