import React, { useState } from "react";
import "./styles/contactUs.css";
import Jumbotron from "react-bootstrap/Jumbotron";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Container";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Carousel from "react-bootstrap/Carousel";
import gatorlogo from "../media/gatorlogo.png";
import "bootstrap/dist/css/bootstrap.min.css";

function contactUs(props) {
  return (
    <Container>
      <Jumbotron className="header">
        <h1 id="titleh1">Contact Us</h1>
        <p>
          Fill out the email form below and we will get back to you as soon as
          possible.
        </p>
        <p>Remember, Code the Gator way!</p>
      </Jumbotron>
      <Form>
        <Form.Group as={Row} controlId="formPlaintextEmail">
          <Form.Label column sm="2">
            Email
          </Form.Label>
          <Col sm="10">
            <Form.Control type="password" placeholder="email@example.com" />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPlaintextPassword">
          <Form.Label column sm="2">
            Subject
          </Form.Label>
          <Col sm="10">
            <Form.Control type="password" placeholder="Subject" />
          </Col>
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Label>Message</Form.Label>
          <Form.Control as="textarea" rows={3} />
        </Form.Group>
      </Form>
    </Container>
  );
}

export default contactUs;
