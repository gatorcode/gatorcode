import React, { useEffect, useState } from "react";
import "./styles/settings.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import defaultProfilePic from "../media/avatars/default.png";
// import avatar1 from "../media/avatars/1.png";
import Axios from "axios";
import { API_URL } from "../constants";

function Settings(props) {
  const [userdata, setUserdata] = useState(props.data);
  const [userfirstname, setUserfirstname] = useState("");
  const [userlastname, setUserlastname] = useState("");
  const [useremail, setUseremail] = useState("");
  const [username, setUsername] = useState("");
  const [userAvatar, setUserAvatar] = useState(0);

  const clearLocalStorage = () => {
    if (localStorage.getItem("0")) {
      localStorage.removeItem("0");
      localStorage.removeItem("0title");
    }
    if (localStorage.getItem("1")) {
      localStorage.removeItem("1");
      localStorage.removeItem("1title");
    }
    if (localStorage.getItem("2")) {
      localStorage.removeItem("2");
      localStorage.removeItem("2title");
    }
    if (localStorage.getItem("3")) {
      localStorage.removeItem("3");
      localStorage.removeItem("3title");
    }
    if (localStorage.getItem("4")) {
      localStorage.removeItem("4");
      localStorage.removeItem("4title");
    }
    if (localStorage.getItem("selected")) {
      localStorage.removeItem("selected");
    }
    if (localStorage.getItem("selectedtitle")) {
      localStorage.removeItem("selectedtitle");
    }
    if (localStorage.getItem("name")) {
      localStorage.removeItem("name");
    }
    if (localStorage.getItem("code")) {
      localStorage.removeItem("code");
    }
  };

  // const setImage = () => {
  //   switch (userAvatar) {
  //     case 1:
  //       return <img id="profilePic" src={defaultProfilePic}></img>;
  //     case 2:
  //       return <img id="profilePic" src={avatar1}></img>;
  //     default:
  //       return <img id="profilePic" src={defaultProfilePic}></img>;
  //   }
  // };

  const updateUser = () => {
    Axios({
      method: "POST",
      data: {
        username: username,
        firstname: userfirstname,
        lastname: userlastname,
        ufemail: useremail,
        avatar: userAvatar
        // major: regmajor,
        // year: regyear
      },
      withCredentials: true,
      url: `${API_URL}/updateuser`
    }).then(res => console.log(res));
  };

  useEffect(() => {
    clearLocalStorage();
    setItems();
  });

  const setItems = async () => {
    try {
      if (localStorage.getItem("usl") == "aaa") {
        const dataobj = await userdata;
        if (dataobj) {
          //console.log(dataobj);
          setUserfirstname(dataobj.data.firstname);
          setUserlastname(dataobj.data.lastname);
          setUseremail(dataobj.data.ufemail);
          setUsername(dataobj.data.username);
          setUserAvatar(dataobj.data.avatar);
        }
      }
    } catch (err) {
      if (err) console.log(err);
    }
  };

  return (
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8 mx-auto">
          <h2 class="h3 mb-4 page-title">Settings</h2>
          <div class="my-4">
            <form>
              {/* <div class="row mt-5 align-items-center">
                <div class="col-md-3 text-center mb-5 justify-content-center">
                  <div class="avatar avatar-xl">
                    <img
                      src={setImage()}
                      alt="..."
                      class="avatar-img rounded-circle"
                    />
                  </div>
                </div>
                <div class="col">
                  <div class="row align-items-center"></div>
                  <div class="row mb-4"></div>
                </div>
              </div> */}
              <hr class="my-4" />
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="firstname">Firstname</label>
                  <input
                    type="text"
                    id="firstname"
                    class="form-control"
                    placeholder={userfirstname}
                  />
                </div>
                <div class="form-group col-md-6">
                  <label for="lastname">Lastname</label>
                  <input
                    type="text"
                    id="lastname"
                    class="form-control"
                    placeholder={userlastname}
                  />
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail4">UF Email</label>
                <input
                  type="email"
                  class="form-control"
                  id="inputEmail4"
                  placeholder={useremail}
                />
              </div>
              <div class="form-group">
                <label for="inputAddress5">Username</label>
                <input
                  type="text"
                  class="form-control"
                  id="inputAddress5"
                  placeholder={username}
                />
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label>Major</label>
                  <select id="selectmajor" class="form-control">
                    <option selected="">Choose...</option>
                    <option>...</option>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label>Avatar</label>
                  <select id="selectavatar" class="form-control">
                    <option selected="">Choose...</option>
                    <option>...</option>
                  </select>
                </div>
              </div>
              <hr class="my-4" />
              {/* <div class="row mb-4">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="inputPassword4">Old Password</label>
                    <input
                      type="password"
                      class="form-control"
                      id="inputPassword5"
                    />
                  </div>
                  <div class="form-group">
                    <label for="inputPassword5">New Password</label>
                    <input
                      type="password"
                      class="form-control"
                      id="inputPassword5"
                    />
                  </div>
                  <div class="form-group">
                    <label for="inputPassword6">Confirm Password</label>
                    <input
                      type="password"
                      class="form-control"
                      id="inputPassword6"
                    />
                  </div>
                </div>
                <div class="col-md-6">
                  <p class="mb-2">Password requirements</p>
                  <p class="small text-muted mb-2">
                    To create a new password, you have to meet all of the
                    following requirements:
                  </p>
                  <ul class="small text-muted pl-4 mb-0">
                    <li>Minimum 8 character</li>
                    <li>At least one special character</li>
                    <li>At least one number</li>
                    <li>Can’t be the same as a previous password</li>
                  </ul>
                </div>
              </div> */}
              <button
                type="submit"
                onClick={updateUser}
                class="btn btn-primary"
              >
                Save Changes
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Settings;
