import React, { useState, useEffect } from "react";
import "./styles/lessons.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import {
  Row,
  Col,
  Accordion,
  Button,
  Card,
  Container,
  Modal,
} from "react-bootstrap";
import Ide from "../components/ide";
import { useParams } from "react-router-dom";
import { API_URL } from "../constants";


function LessonPages(props) {
  const [userdata, setUserdata] = useState(props.data);
  const [lessonTitle, setLessonTitle] = useState(useParams().title);
  const [lessonData, setLessonData] = useState({});

  //states for lesson
  const [checkingAnswer, setCheckingAnswer] = useState(true);
  const [correct, setCorrect] = useState(false);
  const [alreadyComplete, setAlreadyComplete] = useState(false);
  const [incorrect, setIncorrect] = useState(false);
  const [invalidSolution, setInvalidSolution] = useState(false);
  const [descjava, setDescjava] = useState("");
  const [desccpp, setDesccpp] = useState("");
  const [descpython, setDescpython] = useState("");
  const [descc, setDescc] = useState("");
  const [task, setTask] = useState("");
  const [inputType, setInputType] = useState("");
  var userPoints = 0;
  var userLevel = 0;
  var completedLessons = {};


  const getData = () => {
    return Axios({
      method: "post",
      url: `${API_URL}/getlesson`,
      data: {
        title: lessonTitle,
      },
    });
  };

  const completeLesson = async () => {
    const dataobj = await userdata;

    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;

    completedLessons = dataobj.data.lessonsCompleted;

    const complete = dataobj.data.lessonsCompleted[lessonTitle];

    const correctOutput = lessonData.correctOutput;
    const userOutput = document.getElementById("result").value;

    //check for correct output
    if (correctOutput === userOutput) {
      //check for solution keys
      let keys = [];
      switch (localStorage.getItem("lang")) {
        case "java":
          keys = lessonData.keysjava;
          break;
        case "cpp":
          keys = lessonData.keyscpp;
          break;
        case "c":
          keys = lessonData.keysc;
          break;
        case "python":
          keys = lessonData.keyspython;
          break;
      }
      const code = localStorage.getItem("code");
      let validSolution = true;

      keys.map((i, j) => {
        if (!code.includes(keys[j])) {
          validSolution = false;
        }
      });

      if (validSolution) {
        //check for completion status
        if (complete === 0) {
          //add points
          setCheckingAnswer(false);
          setCorrect(true);
          setIncorrect(false);
          //alert("CORRECT!");

          userPoints += 5;

          //mark as complete
          completedLessons[lessonTitle] = 2;

          if (userPoints >= userLevel * 5) {
            userPoints = userPoints - userLevel * 5;
            userLevel += 1;
            addPoints();
          } else {
            addPoints();
          }
          window.location.reload();
        } else {
          setCheckingAnswer(false);
          setAlreadyComplete(true);
          setCorrect(true);
          setIncorrect(false);
        }
      } else {
        setCheckingAnswer(false);
        setIncorrect(true);
        setCorrect(false);

        //alert("INVALID SOLUTION!");
      }
    } else {
      //lesson attempted
      setCheckingAnswer(false);
      setIncorrect(true);
      setCorrect(false);

      //alert("INCORRECT!");
      if (complete !== 2) {
        completedLessons[lessonTitle] = 1;
        addPoints();
      }
    }
  };

  const addPoints = async () => {
    const dataobj = await userdata;

    Axios({
      method: "PUT",
      data: {
        username: dataobj.data.username,
        points: userPoints,
        level: userLevel,
        lessonsCompleted: completedLessons,
      },
      withCredentials: true,
      url: `http://localhost:${process.env.PORT || 5000}/addpointsLessons`,
    });
  };

  const setData = async () => {
    const data = await getData();
    const dataobj = await userdata;
    userPoints = dataobj.data.points;
    userLevel = dataobj.data.level;
    await setLessonData(data.data);
    setCodeTemplateLessons(data.data.inputType);
    let tempDescJava = data.data.descjava;
    let tempDescCpp = data.data.desccpp;
    let tempDescPython = data.data.descpython;
    let tempDescC = data.data.descc;
    let tempTask = data.data.task;
    // await setDesc(tempDesc.replace(/\\n/g, '<br>\n')) <-- for new lines [DOES NOT WORK]
    await setDescjava(tempDescJava);
    await setDesccpp(tempDescCpp);
    await setDescpython(tempDescPython);
    await setDescc(tempDescC);
    await setTask(tempTask);
  };

  //set input value to specified value in lessonData depending on language
  const setInput = () => {
    let input = "";

    switch (localStorage.getItem("lang")) {
      case "java":
        input = lessonData.inputjava;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "cpp":
        input = lessonData.inputcpp;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "c":
        input = lessonData.inputc;
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
      case "python":
        input = lessonData.inputpython;
        console.log(input);
        if (input === undefined) {
          input = "";
        }
        document.getElementById("input").value = input;
        break;
    }
  };

  const setCodeTemplateLessons = (inputTyp) => {
    const input = document.getElementById("input").value;

    const funcName = `${lessonTitle.toLowerCase().replaceAll(" ", "_")}`;

    console.log(inputTyp);

    //!JAVA NOT CURRENTLY WORKING

    const javaTemplate = `public class myClass {

        public static void main (String[] args) {

            for (String s: args) {

                System.out.println(s);

            }

            ${funcName};

        }

    }
    
    public void ${funcName}() {

      //YOUR SOLUTION GOES HERE

    }`;

    //!C NOT CURRENTLY WORKING

    var cTemplate = ``;

    if (inputTyp == "Array") {
      cTemplate = `#include <stdio.h>  
#include <unistd.h>  

void ${funcName}(char *argus[]){

  //printf(argus[0])

  //for (int i = 0; i < 4; ++i)
  // {
  //  printf(argus[i]);
  //}
    

}
  
int main(int argc, char *argv[])  
{ 
    int opt; 
              
    const char *arguments[5];
  
    for(; optind < argc; optind++){      
        arguments[optind] = argv[optind];
        // printf("extra arguments: %s\n", argv[optind]);  
    } 

    ${funcName}(arguments);
    
    return 0; 
} 
      `;
    } else if (
      inputTyp == "String" ||
      inputTyp == "Number" ||
      inputTyp == "Object"
    ) {
      cTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
         
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    } else if (inputTyp == "None") {
      cTemplate = `#include <stdio.h>  
#include <unistd.h>  

void ${funcName}(){

  printf("Hello World!");

}
  
int main(int argc, char *argv[])  
{ 
    ${funcName}(); //your function call
    
    return 0; 
} 
      `;
    }

    var cppTemplate = ``;

    if (inputTyp == "Array") {
      cppTemplate = `#include <iostream>
#include <string>
#include <vector>
      
  
std::vector<std::string> parse_input(std::string args) {
  /*
  !!!DO NOT CHANGE!!!
  This function parses the input arguments and returns a vector<string> of arguments
  */
  
  std::vector<std::string> rect_vect;
  std::string s = args;
  std::string delimiter = ",";
    
  size_t pos = 0;
  std::string token;
  while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    s.erase(0, pos + delimiter.length());
    rect_vect.push_back(token);
  }
      
  return rect_vect;
  
}
      
void ${funcName}(std::vector<std::string> input) {
  /*
  YOUR SOLUTION GOES HERE
  Use '[]' to access elements in input
  i.e. input[0] will access the first element in the 'input' vector
  */
 
  std::cout << "Hello World!" << std::endl;
}
    
int main(int argc, char** argv) { 
  //stores the argumment value(s) from the 'Input' box below
  std::string input = argv[1];
    
  //passes the parsed input vector into your function as a param
  ${funcName}(parse_input(input));
    
  return 0; 

}
  `;
    } else if (
      inputTyp == "String" ||
      inputTyp == "Number" ||
      inputTyp == "Object"
    ) {
      cppTemplate = `#include <iostream>
#include <string>
      
void ${funcName}(std::string argument){
  //YOUR CODE GOES HERE
  std::cout << "Hello World!" << std::endl;

}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  //Parses input and calls your function with the input as a param
  std::string argument = std::string(argv[1]);
  ${funcName}(argument); //your function call
  return 0;
}`;
    } else if (inputTyp == "None") {
      cppTemplate = `#include <iostream>
      
void ${funcName}(){
  //YOUR CODE GOES HERE
  
          
}
      
int main(int argc, char* argv[]){
  //DO NOT CHANGE
  ${funcName}(); //your function call
  return 0;
}`;
    }

    var pythonTemplate = ``;
    if (inputTyp !== "None") {
      pythonTemplate = `import sys

#DO NOT CHANGE
#parse arguments and returns an array
def parse_input():
  return sys.argv[1].split(',')
  
def ${funcName}(parsed_input): 
  #YOUR SOLUTION GOES HERE
  print("Hello World!")

#function call with input as param
${funcName}(parse_input())
`;
    } else {
      pythonTemplate = `import sys
  
def ${funcName}(): 
  #YOUR SOLUTION GOES HERE
  print("Hello World!)

#function call
${funcName}()
`;
    }

    switch (localStorage.getItem("lang")) {
      case "java":
        document.getElementById("code").value = javaTemplate;
        break;
      case "python":
        document.getElementById("code").value = pythonTemplate;
        break;
      case "cpp":
        document.getElementById("code").value = cppTemplate;
        break;
      case "c":
        document.getElementById("code").value = cTemplate;
        break;
    }
  };
  //parse input type
  const handleInputType = () => {};

  const hintLangHandler = () => {
    switch (localStorage.getItem("lang")) {
      case "java":
        return <p1>{lessonData.descjava}</p1>;
      case "python":
        return <p1>{lessonData.descpython}</p1>;
      case "c":
        return <p1>{lessonData.descc}</p1>;
      case "cpp":
        return <p1>{lessonData.desccpp}</p1>;
    }
  };

  const renderStatus = () => {
    if (correct) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <polyline
            class="path check"
            fill="none"
            stroke="#73AF55"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            points="100.2,40.2 51.5,88.8 29.8,67.5 "
          />
        </svg>
      );
    } else if (incorrect) {
      return (
        <svg
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 130.2 130.2"
        >
          <circle
            class="path circle"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-miterlimit="10"
            cx="65.1"
            cy="65.1"
            r="62.1"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="34.4"
            y1="37.9"
            x2="95.8"
            y2="92.3"
          />
          <line
            class="path line"
            fill="none"
            stroke="#D06079"
            stroke-width="6"
            stroke-linecap="round"
            stroke-miterlimit="10"
            x1="95.8"
            y1="38"
            x2="34.4"
            y2="92.2"
          />
        </svg>
      );
    } else {
      return <div className="loader"></div>;
    }
  };

  // const setDescMethod = async () => {
  //   await challengeData;
  //   setDesc(challengeData.desc.replace(/\\n/g, "<br>\n"));
  // };
  const [show, setShow] = useState(true);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  function showModal() {
    return (
      <>
        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          size="xl"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>{lessonData.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{hintLangHandler()}</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" href={`/lessons`}>
              Return to Lessons
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Begin Lesson
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }

  const fillData = () => {
    //setDescMethod();
    return (
      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title id="lessonTitle">{lessonData.title}</Card.Title>
          <Card.Body className="mb-2 text-muted">{task}</Card.Body>
          <Card.Body className="mb-2 text-muted">
            Expected Output: {lessonData.correctOutput}
          </Card.Body>
          {renderSetInputButton()}
        </Card.Body>
        <Button variant="primary" onClick={handleShow}>
          Lesson Description
        </Button>
      </Card>
    );
  };

  const renderSetInputButton = () => {
    if (lessonData.inputjava) {
      return <Button onClick={setInput}>Set Input</Button>;
    } else {
      return (
        <Button id="setInputButton" disabled onClick={setInput}>
          Set Input
        </Button>
      );
    }
  };

  useEffect(() => {
    setData();
  }, []);

  return (
    <Row>
      {showModal()}
      <div className="lessonInfo">
        <Col>
          {fillData()}
          <Row>
            <Container>
              <Button onClick={() => completeLesson()}>Check Answer</Button>
              {renderStatus()}
            </Container>
          </Row>
        </Col>
      </div>
      <div className="sandboxide">
        <Col>
          <Ide />
        </Col>
      </div>
    </Row>
  );
}

export default LessonPages;
