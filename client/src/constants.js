export const API_URL =
  process.env.NODE_ENV === "production"
    ? "https://gatorcode.azurewebsites.net"
    : `http://localhost:5001`; //update with SERVER_PORT

export const COMPILER_PORT = 8013;

export const SERVER_PORT = process.env.PORT || 5001;

export const CLIENT_PORT = process.env.PORT || 3000;
