/*
COMPILER
to run enter "npm start" in terminal
*/

const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const { router: crouter } = require("./langs/c.js");
const { router: cpprouter } = require("./langs/cpp.js");
const { router: pyrouter } = require("./langs/python.js");
const { router: javarouter } = require("./langs/java.js");

const port = process.env.PORT || 8013;

app.use("/code", crouter);
app.use("/code", cpprouter);
app.use("/code", pyrouter);
app.use("/code", javarouter);

app.listen(port, () => {
  console.log(`Compiler listening...`);
});
