/*
Java COMPILER

------------
NOTES:
- may need to handle input
-save to different directory
   - where cannot be found
- add windows support
   - different file systems
*/

//import required packages
const fse = require("fs-extra");
const os = require("os");
const execa = require("execa");
const express = require("express");
const homedir = os.homedir;

//express router
const router = express.Router();

const unixDir = "etc/gatorcode_files";

//function to save file
const saveFile = async (fileName, data) => {
  try {
    await fse.outputFile(`${homedir}/${fileName}`, data);
    //console.log("File saved successfully!");
  } catch (error) {
    console.log("File not saved!");
    console.log(error);
  }
};

//execute java code
const javaExecute = async (code, input, fileName) => {
  console.log(code, input, fileName);

  fileName = "myClass.java";

  try {
    //create java file
    await saveFile(fileName, code);

    const codePath = `${homedir}/${fileName}`;

    //Compile java
    await execa.command("javac " + codePath);

    //cd to WD
    //await execa.command(`cd ${homedir}`);

    //run output .class file with input
    const javaOutput = await execa.command(
      `java ${homedir}/${fileName} ${input ? input : ""}`,
      {
        shell: true
      }
    );

    //remove file after execution
    await execa.command(`rm ${codePath}`);

    //return output
    return javaOutput.stdout;
  } catch (err) {
    return err;
  }
};

//route --------------------------------------------------
router.post("/submitjava/", async (req, res) => {
  const { code, input, lang, fileName } = req.body;
  const data = await javaExecute(code, input, fileName);
  //console.log(data);
  return res.status(200).send(data);
});

module.exports = { router };
