/*
C++ COMPILER

------------
NOTES:
- may need to handle input
-save to different directory
   - where cannot be found
- add windows support
   - different file systems
*/

//import required packages
const fse = require("fs-extra");
const os = require("os");
const execa = require("execa");
const express = require("express");
const homedir = os.homedir;

//express router
const router = express.Router();
const unixDir = "etc/gatorcode_files";

//function to save file
const saveFile = async (fileName, data) => {
  try {
    //create dir gatorcode if does not exist
    //await fse.mkdirp(`${home}`);
    await fse.outputFile(`${homedir}/${fileName}`, data);
    //console.log("File saved successfully!");
  } catch (error) {
    //console.log("File not saved successfully :,( !");
    console.log(error);
  }
};

//execute c++ code
const cppExecute = async (code, input, fileName) => {
  const filename = fileName;
  try {
    //create c++ file
    await saveFile(filename, code);

    const codePath = `${homedir}/${filename}`;

    //Compile c++
    await execa.command("g++ " + codePath);

    //run output 'a.out' with input
    const cppOutput = await execa.command(`./a.out ${input ? input : ""}`, {
      shell: true
    });

    //   await execa.command(`
    // if [ -d "/path/to/dir" ]
    // then
    //   rm ${codePath}
    // fi`);

    //console.log("here");

    //return output
    return cppOutput.stdout;
  } catch (err) {
    return err;
  }
};

//route --------------------------------------------------
router.post("/submitcpp/", async (req, res) => {
  const { code, input, lang, fileName } = req.body;
  const data = await cppExecute(code, input, fileName);
  //console.log(data);
  return res.status(200).send(data);
});

module.exports = { router };
