/*
Python compiler

------------
NOTES:
- may need to handle input
-save to different directory
   - where cannot be found
- add windows support
   - different file systems
*/

//import required packages
const fse = require("fs-extra");
const os = require("os");
const execa = require("execa");
const express = require("express");
const homedir = os.homedir;

//express router
const router = express.Router();
const unixDir = "etc/gatorcode_files";

//function to save file
const saveFile = async (fileName, data) => {
  try {
    //create dir gatorcode if does not exist
    // await fse.mkdirp(`${unixDir}`);
    await fse.outputFile(`${homedir}/${fileName}`, data);
    console.log("File saved successfully!");
  } catch (error) {
    console.log("File not saved successfully :,( !");
    console.log(error);
  }
};

//execute python code
const pyExecute = async (code, input, fileName) => {
  console.log(code, input, fileName);
  const filename = fileName;
  try {
    //create python file
    await saveFile(filename, code);

    const codePath = `${homedir}/${filename}`;

    //Compile python
    const pyOutput = await execa.command(
      `python -u ${codePath} ${input ? input : ""}`,
      {
        shell: true
      }
    );

    //remove file after execution
    await execa.command(`rm ${codePath}`);

    //return output
    //console.log(pyOutput);
    return pyOutput.stdout;
  } catch (err) {
    return err;
  }
};

//route --------------------------------------------------
router.post("/submitpython/", async (req, res) => {
  const { code, input, lang, fileName } = req.body;
  const data = await pyExecute(code, input, fileName);
  console.log(data);
  return res.status(200).send(data);
});

module.exports = { router };
